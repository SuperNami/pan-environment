#!/bin/bash -x

if [ ! -e "/etc/nginx/conf.d/platform.conf" ]; then

  export DOMAIN=${DOMAIN}
  export UPSTREAM=${UPSTREAM}

  cp /etc/nginx/conf.d/default-platform.conf /etc/nginx/conf.d/platform.conf

  sed -i "s/website.tld/${DOMAIN}/" /etc/nginx/conf.d/platform.conf
  sed -i "s/localhost/${UPSTREAM}/" /etc/nginx/conf.d/platform.conf

  touch /var/log/nginx/${DOMAIN}-error.log
  touch /var/log/nginx/default-server-error.log

  ln -sf /dev/stderr /var/log/nginx/${DOMAIN}-error.log
  ln -sf /dev/stderr /var/log/nginx/default-server-error.log

  echo "Nginx has just been configured"

else
  echo "Nginx is already configured"
fi

rm -f /etc/nginx/conf.d/default-platform.conf

exec "$@"