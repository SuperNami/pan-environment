VERSION=5.5.52.2

build: build-pan-7 build-pan-71 build-support


# PAN ioncube 7
build-pan-7: build-pan-base-7 build-pan-base-cron-7 build-pan-files-7

build-pan-base-7:
	docker build -t registry.gitlab.com/supernami/pan-environment/ioncube-php-7/base:${VERSION} -f ioncube-php-7/base.df . && \
	docker tag registry.gitlab.com/supernami/pan-environment/ioncube-php-7/base:${VERSION} registry.gitlab.com/supernami/pan-environment/ioncube-php-7/base:latest

build-pan-base-cron-7:
	docker build -t registry.gitlab.com/supernami/pan-environment/ioncube-php-7/base-cron:${VERSION} -f ioncube-php-7/cron.df . && \
	docker tag registry.gitlab.com/supernami/pan-environment/ioncube-php-7/base-cron:${VERSION} registry.gitlab.com/supernami/pan-environment/ioncube-php-7/base-cron:latest

build-pan-files-7:
	docker build -t registry.gitlab.com/supernami/pan-environment/ioncube-php-7/files:${VERSION} -f ioncube-php-7/files.df . && \
	docker tag registry.gitlab.com/supernami/pan-environment/ioncube-php-7/files:${VERSION} registry.gitlab.com/supernami/pan-environment/ioncube-php-7/files:latest


# PAN ioncube 71
build-pan-71: build-pan-base-71 build-pan-base-cron-71 build-pan-files-71

build-pan-base-71:
	docker build -t registry.gitlab.com/supernami/pan-environment/ioncube-php-71/base:${VERSION} -f ioncube-php-71/base.df . && \
	docker tag registry.gitlab.com/supernami/pan-environment/ioncube-php-71/base:${VERSION} registry.gitlab.com/supernami/pan-environment/ioncube-php-71/base:latest

build-pan-base-cron-71:
	docker build -t registry.gitlab.com/supernami/pan-environment/ioncube-php-71/base-cron:${VERSION} -f ioncube-php-71/cron.df . && \
	docker tag registry.gitlab.com/supernami/pan-environment/ioncube-php-71/base-cron:${VERSION} registry.gitlab.com/supernami/pan-environment/ioncube-php-71/base-cron:latest

build-pan-files-71:
	docker build -t registry.gitlab.com/supernami/pan-environment/ioncube-php-71/files:${VERSION} -f ioncube-php-71/files.df . && \
	docker tag registry.gitlab.com/supernami/pan-environment/ioncube-php-71/files:${VERSION} registry.gitlab.com/supernami/pan-environment/ioncube-php-71/files:latest


# Support containers
build-support: build-nginx build-cron build-percona

build-nginx:
	docker build -t registry.gitlab.com/supernami/pan-environment/nginx:latest -f nginx/nginx.df .

build-cron:
	docker build -t registry.gitlab.com/supernami/pan-environment/cron:latest -f cron/cron.df .

build-percona:
	docker -t registry.gitlab.com/supernami/pan-environment/percona:latest -f percona/percona.df .


# Docker compose up
docker-compose-up:
	docker-compose up