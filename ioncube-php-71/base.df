FROM php:7.1-fpm

ENV TERM=xterm

# Set recommended PHP.ini settings
# See https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

# Platform php dependencies
RUN apt-get update && \
    apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libmcrypt-dev libpng-dev && \
    docker-php-ext-install -j$(nproc) mysqli pdo_mysql mbstring iconv mcrypt && \
    docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ && \
    docker-php-ext-install -j$(nproc) gd

# Ioncube loader
RUN apt-get update && \
    apt-get install -y wget && \
    wget https://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz && \
    tar -zxvf ioncube_loaders_lin_x86-64.tar.gz && \
    mkdir /usr/local/ioncube && \
    cp ioncube/ioncube_loader_lin_7.1.so /usr/local/ioncube/ioncube_loader_lin_7.1.so && \
    echo "zend_extension = /usr/local/ioncube/ioncube_loader_lin_7.1.so" >> /usr/local/etc/php/php.ini && \
    rm ioncube_loaders_lin_x86-64.tar.gz && \
    rm -R ioncube

# Switch off debugging
RUN echo "display_errors = Off" >> /usr/local/etc/php/php.ini

# Turn on debugging
#RUN echo "error_reporting = E_ALL" >> /usr/local/etc/php/php.ini && \
#    echo "display_errors = On" >> /usr/local/etc/php/php.ini

# MSMTP
RUN apt-get update && \
    apt-get install -y automake gcc gettext texinfo ca-certificates libgnutls28-dev libgsasl7-dev libidn11-dev libsecret-1-dev git wget unzip nano && \
    git clone git://git.code.sf.net/p/msmtp/code msmtp && \
    cd msmtp && \
    autoreconf -i && \
    ./configure --prefix=/usr/local && \
    make && \
    make install && \
    touch /var/log/msmtp.log && \
    chmod +x /var/log/msmtp.log && \
    chown -R www-data:adm /var/log/msmtp.log && \
    cd .. && \
    rm -R msmtp && \
    apt-get remove -y automake gcc gettext texinfo && \
    apt-get autoremove -y && \
	apt-get clean

# Copy entrypoint
COPY ioncube-php-71/entrypoint.sh /usr/local/bin
RUN chmod +x /usr/local/bin/entrypoint.sh